
ValaUnit
========

A Glib-based, xUnit-style unit testing and mock object framework for
Vala projects.

Tests typically extend `ValaUnit.TestCase`, see the API documentation
for more information.
